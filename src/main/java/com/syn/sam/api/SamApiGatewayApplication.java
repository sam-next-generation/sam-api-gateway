package com.syn.sam.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulServer;

@SpringBootApplication
@EnableZuulServer
public class SamApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SamApiGatewayApplication.class, args);
	}
}
